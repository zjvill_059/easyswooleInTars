<?php
/**
 * Created by PhpStorm.
 * User: priestll
 * Date: 20-7-16
 * Time: 下午3:03
 */

namespace App\WebSocket;


use App\Biz\AwardsBiz;
use EasySwoole\Component\Di;
use priestll\easyswoole\websocket\Auth;

class Awards extends Auth
{

    public function start()
    {
        $user = $this->getUser();

        $config = -80;
        $baseconfig = 100 - $config;

        $rand = rand(0, 100);
        if ($rand < $baseconfig) {
            $biz = Di::getInstance()->get('AWARDS_BIZ');
            $biz->cache($user->id);
        } else {
            $data['type'] = AwardsBiz::TYPE_NONE;
            $data['name'] = '谢谢参与';
            $this->success($data);
        }
    }

}