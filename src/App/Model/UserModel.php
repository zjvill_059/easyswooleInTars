<?php

namespace App\Model;


/**
 * UserModel
 * Class UserModel
 * Create With ClassGeneration
 * @property int $id //
 * @property string $username //
 * @property string $password_hash //
 * @property string $password_reset_token //
 * @property string $auth_key //
 * @property string $name //
 * @property string $addr //
 * @property string $tel //
 * @property string $email //
 * @property int $role //
 * @property int $status //
 * @property string $nickname //
 * @property string $avatar //
 * @property string $openid //
 * @property string $unionid //
 * @property string $wx_openid // 公众号openid
 * @property string $mini_openid // 小程序openid
 * @property int $is_follow //
 * @property int $re_authorization //
 * @property string $session_key //
 * @property string $access_token //
 * @property int $expire_at //
 * @property int $created_at //
 * @property int $updated_at //
 */
class UserModel extends BaseModel
{
	protected $tableName = 'user';

    protected $autoTimeStamp = true;
    protected $createTime = 'created_at';
    protected $updateTime = 'updated_at';




	public function getList(int $page = 1, int $pageSize = 10, string $field = '*'): array
	{
		$list = $this
		    ->withTotalCount()
			->order($this->schemaInfo()->getPkFiledName(), 'DESC')
		    ->field($field)
		    ->page($page, $pageSize)
		    ->all();
		$total = $this->lastQueryResult()->getTotalCount();;
		return ['total' => $total, 'list' => $list];
	}
}

