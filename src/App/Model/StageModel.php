<?php

namespace App\Model;


/**
 * StageModel
 * Class StageModel
 * Create With ClassGeneration
 * @property int $id //
 * @property string $name // 名称
 * @property int $power // 所需能量
 * @property int $beg_time // 开始时间
 * @property int $end_time // 截止时间
 * @property int $created_at // 创建时间
 * @property int $updated_at // 更新时间
 * @property int $awards_id // 奖品ID
 */
class StageModel extends BaseModel
{
	protected $tableName = 'jrhd_stage';

    protected $autoTimeStamp = true;
    protected $createTime = 'created_at';
    protected $updateTime = 'updated_at';




	public function getList(int $page = 1, int $pageSize = 10, string $field = '*'): array
	{
		$list = $this
		    ->withTotalCount()
			->order($this->schemaInfo()->getPkFiledName(), 'DESC')
		    ->field($field)
		    ->page($page, $pageSize)
		    ->all();
		$total = $this->lastQueryResult()->getTotalCount();;
		return ['total' => $total, 'list' => $list];
	}

	public function getAll(){
        return StageModel::create()
                ->field([
                    'id',
                    'name',
                    'power',
                    'beg_time',
                    'end_time',
                ])
                ->all();
    }

    public function awards()
    {
        return $this->hasOne(AwardsModel::class, null, 'awards_id', 'id');
    }




}

