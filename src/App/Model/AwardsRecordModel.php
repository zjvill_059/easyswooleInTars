<?php

namespace App\Model;

use EasySwoole\ORM\AbstractModel;

/**
 * AwardsRecordModel
 * Class AwardsRecordModel
 * Create With ClassGeneration
 * @property int $id //
 * @property int $user_id // 用户id
 * @property int $award_id // 奖项id
 * @property string $award_name //
 * @property int $status // 是否中奖
 * @property int $created_at //
 * @property int $updated_at //
 * @property int $is_send // 是否核销
 * @property string $mch_billno // 红包发放单号
 */
class AwardsRecordModel extends BaseModel
{
	protected $tableName = 'jrhd_awards_record';

    protected $autoTimeStamp = true;

	public function getList(int $page = 1, int $pageSize = 10, string $field = '*'): array
	{
		$list = $this
		    ->withTotalCount()
			->order($this->schemaInfo()->getPkFiledName(), 'DESC')
		    ->field($field)
		    ->page($page, $pageSize)
		    ->all();
		$total = $this->lastQueryResult()->getTotalCount();;
		return ['total' => $total, 'list' => $list];
	}
}

