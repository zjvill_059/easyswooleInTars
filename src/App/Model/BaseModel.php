<?php

namespace App\Model;

use EasySwoole\ORM\AbstractModel;
use EasySwoole\ORM\DbManager;

/**
 * BaseModel
 * Class BaseModel
 * Create With ClassGeneration
 */
class BaseModel extends AbstractModel
{
    protected $createTime = 'created_at';
    protected $updateTime = 'updated_at';

	public static function transaction(callable $callable)
	{
		try {
		    DbManager::getInstance()->startTransaction();
		    $result = $callable();
		    DbManager::getInstance()->commit();
		    return $result;
		} catch (\Throwable $throwable) {
		    DbManager::getInstance()->rollback();
		    throw $throwable;;
		}
	}
}

