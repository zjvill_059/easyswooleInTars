<?php

namespace App\Model;

use EasySwoole\ORM\AbstractModel;

/**
 * StageUserModel
 * Class StageUserModel
 * Create With ClassGeneration
 * @property int $id //
 * @property int $user_id // 用户ID
 * @property int $stage_id // 阶段ID
 * @property int $power // 能量值
 * @property int $created_at // 创建时间
 * @property int $updated_at // 更新时间
 */
class StageUserModel extends BaseModel
{
    protected $tableName = 'jrhd_stage_user';

    protected $autoTimeStamp = true;


    public function stage()
    {
        return $this->hasOne(StageModel::class, null, 'stage_id', 'id')->field([
            'id',
            'name',
            'power',
            'awards_id',
            'beg_time',
            'end_time',
        ]);

    }

    public function getList(int $page = 1, int $pageSize = 10, string $field = '*'): array
    {
        $list = $this
            ->withTotalCount()
            ->order($this->schemaInfo()->getPkFiledName(), 'DESC')
            ->field($field)
            ->page($page, $pageSize)
            ->all();
        $total = $this->lastQueryResult()->getTotalCount();;
        return ['total' => $total, 'list' => $list];
    }

    public function getByUser($user_id)
    {
        $data = $this->field([
            'id',
            'power',
            'stage_id',
        ])
            ->where(['user_id' => $user_id])
            ->get();
        $data->stage()->field([
            'id',
            'name',
            'power',
            'beg_time',
            'end_time',
        ]);

        $res = $data
            ->toArray(false, false);

        return $res;

    }

    public function getByWhere($where)
    {
        return $this->field([
            'id',
            'power',
            'stage_id',
        ])
            ->where($where)
            ->get();
    }


}

