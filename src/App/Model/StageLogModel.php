<?php

namespace App\Model;

use EasySwoole\ORM\AbstractModel;

/**
 * StageLogModel
 * Class StageLogModel
 * Create With ClassGeneration
 * @property int $id //
 * @property int $user_id // 用户ID
 * @property int $assist_id // 助力ID
 * @property int $power // 助力能量
 * @property int $created_at // 更新时间
 */
class StageLogModel extends BaseModel
{
	protected $tableName = 'jrhd_stage_log';


	public function getList(int $page = 1, int $pageSize = 10, string $field = '*'): array
	{
		$list = $this
		    ->withTotalCount()
			->order($this->schemaInfo()->getPkFiledName(), 'DESC')
		    ->field($field)
		    ->page($page, $pageSize)
		    ->all();
		$total = $this->lastQueryResult()->getTotalCount();;
		return ['total' => $total, 'list' => $list];
	}
}

