<?php

namespace App\Model;

use EasySwoole\ORM\AbstractModel;

/**
 * AwardsModel
 * Class AwardsModel
 * Create With ClassGeneration
 * @property int $id //
 * @property string $name // 奖品名称
 * @property int $stock // 库存
 * @property int $is_award // 是否中奖
 * @property int $probability // 中间概率
 * @property int $status // 状态
 * @property string $img // 图片
 * @property int $type // 奖品类型
 * @property float $num // 奖品数据或金额
 * @property string $explain // 奖品说明
 */
class AwardsModel extends BaseModel
{
	protected $tableName = 'jrhd_awards';




	public function getList(int $page = 1, int $pageSize = 10, string $field = '*'): array
	{


		$list = $this
		    ->withTotalCount()
			->order($this->schemaInfo()->getPkFiledName(), 'DESC')
		    ->field($field)
		    ->page($page, $pageSize)
		    ->all();
		$total = $this->lastQueryResult()->getTotalCount();;
		return ['total' => $total, 'list' => $list];
	}
}

