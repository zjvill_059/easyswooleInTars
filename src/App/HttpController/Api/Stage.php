<?php
/**
 * Created by PhpStorm.
 * User: priestll
 * Date: 20-7-15
 * Time: 上午10:30
 */

namespace App\HttpController\Api;

use priestll\easyswoole\http\Auth;

class Stage extends Auth
{
    protected $NO_AUTH_LIST = ['getStage'];

    /**
     * @Di(key="STAGE_BIZ")
     */
    protected $biz;

    /**
     * @Api(name="stage",group="/api/stage",description="获取",path="/api/stage")
     * @Method(allow={GET})
     * @ResponseParam(name="code",description="状态码")
     * @ResponseParam(name="result",description="api请求结果")
     * @ResponseParam(name="msg",description="api提示信息")
     * @ApiSuccess({"code":200,"result":[],"msg":"SUCCESS"})
     * @ApiFail({"code":400,"result":[],"msg":"FAIL"})
     */
    public function index()
    {
        $res = $this->biz->getStage();
        if ($res['status']) {
            return $this->success($res['data']);
        } else {
            return $this->fail($res['data']);
        }
    }

    /**
     * @Api(name="getUserStage",group="/api/stage",description="获取",path="/api/stage/getUserStage")
     * @ApiAuth(name="authorization",from={'HEADER'},description="token")
     * @Method(allow={GET})
     * @ResponseParam(name="code",description="状态码")
     * @ResponseParam(name="result",description="api请求结果")
     * @ResponseParam(name="msg",description="api提示信息")
     * @ApiSuccess({"code":200,"result":[],"msg":"SUCCESS"})
     * @ApiFail({"code":400,"result":[],"msg":"FAIL"})
     */
    public function getUserStage()
    {
        $user = $this->getUser();

        $res = $this->biz->getUserStage($user['id']);
        if ($res['status']) {
            return $this->success($res['data']);
        } else {
            return $this->fail($res['data']);
        }
    }

    /**
     * @Api(name="assist",group="/api/stage",description="获取",path="/api/stage/assist")
     * @ApiAuth(name="authorization",type=string,from={'HEADER'},description="token")
     * @Method(allow={GET})
     * @ResponseParam(name="code",description="状态码")
     * @ResponseParam(name="result",description="api请求结果")
     * @ResponseParam(name="msg",description="api提示信息")
     * @ApiSuccess({"code":200,"result":[],"msg":"SUCCESS"})
     * @ApiFail({"code":400,"result":[],"msg":"FAIL"})
     * @Param(name="assist_id",alias="助力ID",type=integer,required="",integer="")
     */
    public function assist()
    {

        $param = $this->request()->getRequestParam();

        $user = $this->getUser();

        $res = $this->biz->assist($user['id'], $param['assist_id']);

        if ($res['status']) {
            return $this->success($res['data']);
        } else {
            return $this->fail($res['data']);
        }
    }

}