<?php


namespace App\HttpController;

use priestll\easyswoole\http\Auth;

class Index extends Auth
{

    protected $NO_AUTH_LIST = ['test'];

    public function test(){
        return $this->success('test');
    }

}