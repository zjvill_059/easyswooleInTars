<?php
/**
 * Created by PhpStorm.
 * User: priestll
 * Date: 20-7-14
 * Time: 下午4:37
 */

namespace App\HttpController\Common;


use EasySwoole\WeChat\Bean\OfficialAccount\JsAuthRequest;
use priestll\easyswoole\http\Base;
use priestll\easyswoole\wechat\WeChatManager;

class Login extends Base
{
    /**
     * @Di(key="USER_BIZ")
     */
    protected $userbiz;

    /**
     * @Api(name="mobile",group="/common/login",description="手机登陆",path="/common/login/mobile")
     * @Method(allow={POST})
     * @ResponseParam(name="code",description="状态码")
     * @ResponseParam(name="result",description="api请求结果")
     * @ResponseParam(name="msg",description="api提示信息")
     * @ApiSuccess({"code":200,"result":[],"msg":"SUCCESS"})
     * @ApiFail({"code":400,"result":[],"msg":"FAIL"})
     * @Param(name="mobile",alias="手机号码",required="",lengthMax="11",'')
     */
    public function mobile()
    {
        $param = $this->request()->getRequestParam();

        $res = $this->userbiz->loginByMobile($param['mobile']);

        if ($res['status']) {
            return $this->success($res['data']);
        } else {
            return $this->fail($res['data']);
        }
    }


    public function h5(){

        $weChat = WeChatManager::getInstance()->weChat('wechat');

        // 获取JsApi对象
        $jsApi = $weChat->officialAccount()->jsApi();

        // 创建微信授权跳转连接
        $jsAuthRequest = new JsAuthRequest();
        // 设置授权后回调地址
        $jsAuthRequest->setRedirectUri('http://m.easyswoole.cn');
        // 设置 state
        $jsAuthRequest->setState('test');
        // 设置授权类型
        $jsAuthRequest->setType($jsAuthRequest::TYPE_USER_INFO);
        // 返回生成地址 需要开发者自行重定向用户
        $link = $jsApi->auth()->generateURL($jsAuthRequest);

    }

}