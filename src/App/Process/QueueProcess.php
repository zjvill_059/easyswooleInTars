<?php
/**
 * Created by PhpStorm.
 * User: priestll
 * Date: 20-7-17
 * Time: 上午9:54
 */

namespace App\Process;


use EasySwoole\Component\Di;
use EasySwoole\Component\Process\AbstractProcess;
use priestll\easyswoole\queue\Job;
use priestll\easyswoole\queue\Queue;


class QueueProcess extends AbstractProcess
{

    protected function run($arg)
    {
        go(function (){
            Queue::getInstance()->consumer()->listen(function (Job $job){

                $di = Di::getInstance();
                if( $job->getMethod() == 'awards'){
                    $biz = $di->get('AWARDS_BIZ');
                    $biz->awards($job->getJobData());
                }

            });
        });
    }
}