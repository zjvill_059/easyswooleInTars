<?php
/**
 * Created by PhpStorm.
 * User: priestll
 * Date: 20-7-14
 * Time: 下午3:05
 */

namespace App\Biz;


use App\Model\UserModel;
use priestll\easyswoole\biz\BaseBiz;
use priestll\easyswoole\util\Token;

class UserBiz extends BaseBiz
{

    public function test(){

        return 'ok';
    }

    /**
     * 账号密码登陆
     */
    public function login(){

    }

    /**
     * 手机登陆
     */
    public function loginByMobile($mobile){
        $user = UserModel::create()
                ->field([
                    'id',
                    'username',
                    'name',
                    'nickname',
                    'avatar',
                    'openid',
                    'unionid',
                    'wx_openid',
                    'mini_openid',
                    'mobile',
                ])
                ->get([
                    'mobile'=>$mobile
                ]);
        if($user){
            $token = Token::getInstance();
            $str = $token->create($user);
            return $this->success($str);
        }else{
            return $this->fail('登陆失败');
        }
    }

    /**
     * 微信H5登陆
     */
    public function loginByH5(){

    }

    /**
     * 微信小程序登陆
     */
    public function loginByMiniprogram(){

    }


}