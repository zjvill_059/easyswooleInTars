-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2020-07-20 01:48:46
-- 服务器版本： 5.6.47-log
-- PHP 版本： 7.2.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `jrminhd2020`
--

-- --------------------------------------------------------

--
-- 表的结构 `jrhd_awards`
--

CREATE TABLE `jrhd_awards` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL COMMENT '奖品名称',
  `stock` int(11) NOT NULL COMMENT '库存',
  `is_award` tinyint(1) NOT NULL COMMENT '是否中奖',
  `probability` int(11) NOT NULL COMMENT '中间概率',
  `status` tinyint(1) NOT NULL COMMENT '状态',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `type` int(1) DEFAULT '0' COMMENT '奖品类型',
  `num` decimal(10,2) DEFAULT NULL COMMENT '奖品数据或金额',
  `explain` varchar(255) DEFAULT '' COMMENT '奖品说明'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `jrhd_awards`
--

INSERT INTO `jrhd_awards` (`id`, `name`, `stock`, `is_award`, `probability`, `status`, `img`, `type`, `num`, `explain`) VALUES
(1, '实物', 9364, 1, 80, 0, 'http://oss30.oss-cn-shenzhen.aliyuncs.com/kpjs/lottery/8fb7cb9a99a87bceeb2494ba380e339f.jpg', 1, '5.00', '5积分');

-- --------------------------------------------------------

--
-- 表的结构 `jrhd_awards_record`
--

CREATE TABLE `jrhd_awards_record` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `award_id` int(11) NOT NULL COMMENT '奖项id',
  `award_name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '是否中奖',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `is_send` tinyint(1) NOT NULL COMMENT '是否核销',
  `mch_billno` varchar(255) DEFAULT '' COMMENT '红包发放单号'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `jrhd_stage`
--

CREATE TABLE `jrhd_stage` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `power` int(11) DEFAULT '10' COMMENT '所需能量',
  `awards_id` int(11) DEFAULT NULL COMMENT '奖品ID',
  `beg_time` int(11) DEFAULT '0' COMMENT '开始时间',
  `end_time` int(11) DEFAULT '0' COMMENT '截止时间',
  `created_at` int(11) DEFAULT '0' COMMENT '创建时间',
  `updated_at` int(11) DEFAULT '0' COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 转存表中的数据 `jrhd_stage`
--

INSERT INTO `jrhd_stage` (`id`, `name`, `power`, `awards_id`, `beg_time`, `end_time`, `created_at`, `updated_at`) VALUES
(1, '第一站', 10, 1, 1594717378, 1597395778, 1594717378, 1594717378),
(2, '第二站', 20, 2, 1594717378, 1597395778, 1597395778, 1597395778),
(3, '第三站', 30, 3, 1594717378, 1597395778, 1594717378, 1594717378),
(4, '第四站', 40, 4, 1594717378, 1597395778, 1597395778, 1597395778),
(5, '第五站', 50, 5, 1594717378, 1597395778, 1594717378, 1594717378),
(6, '第六站', 60, 6, 1594717378, 1597395778, 1597395778, 1597395778);

-- --------------------------------------------------------

--
-- 表的结构 `jrhd_stage_log`
--

CREATE TABLE `jrhd_stage_log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `assist_id` int(11) NOT NULL COMMENT '助力ID',
  `power` int(11) DEFAULT '0' COMMENT '助力点数',
  `created_at` int(11) DEFAULT '0' COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- 表的结构 `jrhd_stage_user`
--

CREATE TABLE `jrhd_stage_user` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `stage_id` int(11) NOT NULL COMMENT '阶段ID',
  `power` int(11) DEFAULT '0' COMMENT '能量值',
  `created_at` int(11) DEFAULT '0' COMMENT '创建时间',
  `updated_at` int(11) DEFAULT '0' COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `auth_key` varchar(32) NOT NULL,
  `name` varchar(5) DEFAULT '',
  `addr` varchar(255) DEFAULT NULL,
  `mobile` varchar(11) DEFAULT '' COMMENT '手机号码',
  `email` varchar(255) NOT NULL,
  `role` smallint(6) NOT NULL DEFAULT '10',
  `status` smallint(6) NOT NULL DEFAULT '10',
  `nickname` varchar(128) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `openid` varchar(32) NOT NULL,
  `unionid` varchar(32) NOT NULL,
  `wx_openid` varchar(120) DEFAULT NULL COMMENT '公众号openid',
  `mini_openid` varchar(120) DEFAULT NULL COMMENT '小程序openid',
  `is_follow` smallint(1) DEFAULT '0',
  `re_authorization` tinyint(1) NOT NULL DEFAULT '0',
  `session_key` varchar(32) NOT NULL DEFAULT '',
  `access_token` varchar(255) NOT NULL DEFAULT '',
  `expire_at` int(11) NOT NULL DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 转存表中的数据 `user`
--

INSERT INTO `user` (`id`, `username`, `password_hash`, `password_reset_token`, `auth_key`, `name`, `addr`, `mobile`, `email`, `role`, `status`, `nickname`, `avatar`, `openid`, `unionid`, `wx_openid`, `mini_openid`, `is_follow`, `re_authorization`, `session_key`, `access_token`, `expire_at`, `created_at`, `updated_at`) VALUES
(10000, 'test', '', NULL, '', '', NULL, '13827215563', '', 10, 10, NULL, NULL, '', '', NULL, NULL, 0, 0, '', '123456', 0, 0, 0),
(10001, 'priestll', '', NULL, '', '', NULL, '13827215564', '', 10, 10, NULL, NULL, '', '', NULL, NULL, 0, 0, '', '', 0, 0, 0);

--
-- 转储表的索引
--

--
-- 表的索引 `jrhd_awards`
--
ALTER TABLE `jrhd_awards`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `jrhd_awards_record`
--
ALTER TABLE `jrhd_awards_record`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- 表的索引 `jrhd_stage`
--
ALTER TABLE `jrhd_stage`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `jrhd_stage_log`
--
ALTER TABLE `jrhd_stage_log`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `jrhd_stage_user`
--
ALTER TABLE `jrhd_stage_user`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `jrhd_awards`
--
ALTER TABLE `jrhd_awards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `jrhd_awards_record`
--
ALTER TABLE `jrhd_awards_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `jrhd_stage`
--
ALTER TABLE `jrhd_stage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- 使用表AUTO_INCREMENT `jrhd_stage_log`
--
ALTER TABLE `jrhd_stage_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `jrhd_stage_user`
--
ALTER TABLE `jrhd_stage_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10002;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
